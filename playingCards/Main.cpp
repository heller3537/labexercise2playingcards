#include <iostream>
#include <conio.h>

// Aaron Heller
// Lab exercise 2: Playing Cards

using namespace std;

// This code was in the original project that I forked, but I am not seeing why it is needed.
//string ranks[] = {
//	"two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king", "ace"
//};

//string suits[] = { "spades", "hearts", "clubs", "diamonds" };

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADES, HEARTS, DIAMONDS, CLUBS
};

struct Card
{
	Rank rank;
	Suit suit;
};

// This function prints the rank and suit rather than numbers for the rank and suit
void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case TWO: cout << "two"; break;
	case THREE: cout << "three"; break;
	case FOUR: cout << "four"; break;
	case FIVE: cout << "five"; break;
	case SIX: cout << "six"; break;
	case SEVEN: cout << "seven"; break;
	case EIGHT: cout << "eight"; break;
	case NINE: cout << "nine"; break;
	case TEN: cout << "ten"; break;
	case JACK: cout << "jack"; break;
	case QUEEN: cout << "queen"; break;
	case KING: cout << "king"; break;
	case ACE: cout << "ace"; break;
	}

	cout << " of ";
	switch (card.suit)
	{
	case HEARTS: cout << "hearts\n"; break;
	case CLUBS: cout << "clubs\n"; break;
	case DIAMONDS: cout << "diamonds\n"; break;
	case SPADES: cout << "spades\n"; break;
	}
}

// This is a function that returns the higher card
Card HighCard(Card c2, Card c3)
{
	if (c2.rank > c3.rank) return c2;
	if (c2.rank < c3.rank) return c3;
}

int main()
{
	Card c1;
	Card c2;

	c1.rank = TEN;
	c1.suit = SPADES;

	c2.rank = QUEEN;
	c2.suit = CLUBS;

	// Prints the c1 rank and suit
	PrintCard(c1);
	cout << endl;

	// Prints the higher card of either c1 or c2
	PrintCard(HighCard(c1, c2));
	
	
	_getch();
	return 0;
}